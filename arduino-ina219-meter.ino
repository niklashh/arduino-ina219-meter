#include <ArduinoJson.h>
#include <Wire.h>
#include <Adafruit_INA219.h>

const int loop_delay = 30 * 1000;
const int capacity = JSON_OBJECT_SIZE(6);

StaticJsonDocument<capacity> doc;
Adafruit_INA219 ina219;

long readVcc() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2);
  // Wait for Vref to settle
  ADCSRA |= _BV(ADSC);
  // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1126400L / result;
  // Back-calculate AVcc in mV
  return result;
}

void setup() {
  Serial.begin(115200);
  // Initialize the INA219.
  ina219.begin();
  ina219.setCalibration_16V_400mA();
}

void loop() {
  float shuntvoltage = ina219.getShuntVoltage_mV();
  float busvoltage = ina219.getBusVoltage_V();
  float current_mA = ina219.getCurrent_mA();
  float power_mW = ina219.getPower_mW();
  float loadvoltage = busvoltage + (shuntvoltage / 1000);
  double resistance = (1000 * busvoltage) / current_mA;

  doc["voltage_bus_V"] = busvoltage;
  doc["voltage_shunt_mV"] = shuntvoltage;
  doc["voltage_usb_mV"] = readVcc();
  doc["current_mA"] = current_mA;
  doc["power_mW"] = power_mW;
  doc["resistance_ohms"] = resistance;

  String json;
  serializeJson(doc, json);

  Serial.println(json);

  delay(loop_delay);
}
